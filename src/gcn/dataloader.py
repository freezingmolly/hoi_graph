# --------------------------------------------------------
# Modify based on Tensorflow iCAN
# --------------------------------------------------------

"""
Generating training instances
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import json
import pickle
import random
from random import randint
import tensorflow as tf
import cv2
from config_vcoco import cfg
import ipdb

from utils import *

def encode_boxes(boxes_H, boxes_O):
    # relative spatial encoding
    assert len(boxes_H)==len(boxes_O)
    H_encode = bbox_transform(np.array(boxes_O),np.array(boxes_H))
    O_encode = bbox_transform(np.array(boxes_H),np.array(boxes_O))
    HO_encode = H_encode - O_encode
    
    return H_encode, O_encode, HO_encode

def bb_IOU(boxA, boxB):

    ixmin = np.maximum(boxA[0], boxB[0])
    iymin = np.maximum(boxA[1], boxB[1])
    ixmax = np.minimum(boxA[2], boxB[2])
    iymax = np.minimum(boxA[3], boxB[3])
    iw = np.maximum(ixmax - ixmin + 1., 0.)
    ih = np.maximum(iymax - iymin + 1., 0.)
    inters = iw * ih

    # union
    uni = ((boxB[2] - boxB[0] + 1.) * (boxB[3] - boxB[1] + 1.) +
           (boxA[2] - boxA[0] + 1.) *
           (boxA[3] - boxA[1] + 1.) - inters)

    overlaps = inters / uni
    return overlaps

def Augmented_box(bbox, shape, image_id, augment = 15):

    thres_ = 0.7

    box = np.array([0, bbox[0],  bbox[1],  bbox[2],  bbox[3]]).reshape(1,5) #[x1, y1, x2, y2]
    box = box.astype(np.float64)
        
    count = 0
    time_count = 0
    while count < augment:
        
        time_count += 1
        height = bbox[3] - bbox[1]
        width  = bbox[2] - bbox[0]

        height_cen = (bbox[3] + bbox[1]) / 2
        width_cen  = (bbox[2] + bbox[0]) / 2

        ratio = 1 + randint(-10,10) * 0.01

        height_shift = randint(-np.floor(height),np.floor(height)) * 0.1
        width_shift  = randint(-np.floor(width),np.floor(width)) * 0.1

        H_0 = max(0, width_cen + width_shift - ratio * width / 2)
        H_2 = min(shape[1] - 1, width_cen + width_shift + ratio * width / 2)
        H_1 = max(0, height_cen + height_shift - ratio * height / 2)
        H_3 = min(shape[0] - 1, height_cen + height_shift + ratio * height / 2)
        
        
        if bb_IOU(bbox, np.array([H_0, H_1, H_2, H_3])) > thres_:
            box_ = np.array([0, H_0, H_1, H_2, H_3]).reshape(1,5)
            box  = np.concatenate((box,     box_),     axis=0)
            count += 1
        if time_count > 150:
            return box
            
    return box

def Generate_action_VCOCO(action_list):
    action_ = np.zeros(26)
    for GT_idx in action_list:
        action_[GT_idx] = 1
    action_ = action_.reshape(1,26)
    return action_


def Get_Next_Instance_HO_Neg(trainval_GT, Trainval_Neg, iter, Pos_augment, Neg_select, Data_length):
    # for each human-object pair sample to form a batch
    GT       = trainval_GT[iter%Data_length]
    image_id = GT[0]
    if Trainval_Neg:
        im_file  = cfg.DATA_DIR + '/' + 'v-coco/images/train2014/COCO_train2014_' + (str(image_id)).zfill(12) + '.jpg'
    else:
        im_file  = cfg.DATA_DIR + '/' + 'v-coco/images/test/' + (str(image_id)).zfill(6) + '.jpg'
    if not os.path.exists(im_file):
        print(iter, im_file)
    im       = cv2.imread(im_file)
    im_orig  = im.astype(np.float32, copy=True)
    im_orig -= cfg.PIXEL_MEANS
    im_shape = im_orig.shape
    im_orig  = im_orig.reshape(1, im_shape[0], im_shape[1], 3)


    H_augmented, H_augmented_solo, O_augmented, action_HO, action_H, mask_HO, mask_H, H_aug_enc, O_aug_enc, HO_aug_enc = Augmented_HO_Neg(GT, Trainval_Neg, im_shape, Pos_augment, Neg_select)
      
    
    blobs = {}
    blobs['image']       = im_orig
    blobs['H_boxes_solo']= H_augmented_solo
    blobs['H_boxes']     = H_augmented
    blobs['O_boxes']     = O_augmented
    blobs['gt_class_HO'] = action_HO
    blobs['gt_class_H']  = action_H
    blobs['Mask_HO']     = mask_HO
    blobs['Mask_H']      = mask_H
    blobs['H_num']       = len(action_H)
    blobs['H_augmented_enc']  = H_aug_enc
    blobs['O_augmented_enc']  = O_aug_enc 
    blobs['HO_augmented_enc'] = HO_aug_enc

    return blobs

def Augmented_HO_Neg(GT, Trainval_Neg, shape, Pos_augment, Neg_select):
    image_id = GT[0]
    Human    = GT[2]
    Object   = GT[3]

    action_HO_ = Generate_action_VCOCO(GT[1])
    action_H_  = Generate_action_VCOCO(GT[4]) 
    mask_HO_   = np.asarray([1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1]).reshape(1,26)
    mask_H_    = np.asarray([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]).reshape(1,26)
    
    if not Trainval_Neg: # Testval no augment and negative sampling
        Human_augmented = np.array([0, Human[0], Human[1], Human[2], Human[3]]).reshape(1,5)
        Human_augmented.astype(np.float64)
        Object_augmented = np.array([0, Object[0], Object[1], Object[2], Object[3]]).reshape(1,5)
        Object_augmented.astype(np.float64)
        Human_augmented_solo = Human_augmented.copy()
        
        num = len(Human_augmented)
        H_encode, O_encode, HO_encode = encode_boxes(Human_augmented[:,1:], Object_augmented[:,1:])
        
        Human_augmented   = Human_augmented.reshape( num, 5 )
        Human_augmented_solo = Human_augmented_solo.reshape( num, 5 ) 
        Object_augmented  = Object_augmented.reshape( num, 5 ) 
        action_HO         = action_HO_.reshape(num, 26 ) 
        action_H          = action_H_.reshape( num, 26 )
        mask_HO           = mask_HO_.reshape(  num, 26 )
        mask_H            = mask_H_.reshape(   num, 26 )
        H_augmented_encode  = H_encode.reshape( num, 4 )
        O_augmented_encode  = O_encode.reshape( num, 4 )
        HO_augmented_encode = HO_encode.reshape( num, 4 )
        
    else:
        Human_augmented  = Augmented_box(Human,  shape, image_id, Pos_augment)
        Object_augmented = Augmented_box(Object, shape, image_id, Pos_augment)
        Human_augmented_solo = Human_augmented.copy() # only positive

        Human_augmented  =  Human_augmented[:min(len(Human_augmented),len(Object_augmented))]
        Object_augmented = Object_augmented[:min(len(Human_augmented),len(Object_augmented))]


        num_pos = len(Human_augmented)


        if image_id in Trainval_Neg:

            if len(Trainval_Neg[image_id]) < Neg_select:
                for Neg in Trainval_Neg[image_id]:
                    Human_augmented  = np.concatenate((Human_augmented,  np.array([0, Neg[2][0], Neg[2][1], Neg[2][2], Neg[2][3]]).reshape(1,5)), axis=0)
                    Object_augmented = np.concatenate((Object_augmented, np.array([0, Neg[3][0], Neg[3][1], Neg[3][2], Neg[3][3]]).reshape(1,5)), axis=0)
            else:
                List = random.sample(range(len(Trainval_Neg[image_id])), len(Trainval_Neg[image_id]))
                for i in range(Neg_select):
                    Neg = Trainval_Neg[image_id][List[i]]
                    Human_augmented  = np.concatenate((Human_augmented,  np.array([0, Neg[2][0], Neg[2][1], Neg[2][2], Neg[2][3]]).reshape(1,5)), axis=0)
                    Object_augmented = np.concatenate((Object_augmented, np.array([0, Neg[3][0], Neg[3][1], Neg[3][2], Neg[3][3]]).reshape(1,5)), axis=0)

        num_pos_neg = len(Human_augmented) # <=46


        action_HO = action_HO_
        action_H  = action_H_
        mask_HO   = mask_HO_
        mask_H    = mask_H_


        for i in range(num_pos - 1):
            action_HO = np.concatenate((action_HO, action_HO_), axis=0)
            action_H  = np.concatenate((action_H,  action_H_),  axis=0)
            mask_H    = np.concatenate((mask_H,    mask_H_),    axis=0)

        for i in range(num_pos_neg - 1):
            mask_HO   = np.concatenate((mask_HO,   mask_HO_),   axis=0)

        for i in range(num_pos_neg - num_pos):
            action_HO = np.concatenate((action_HO, np.zeros(26).reshape(1,26)), axis=0)

        H_encode, O_encode, HO_encode = encode_boxes(Human_augmented[:,1:], Object_augmented[:,1:])

        Human_augmented   = Human_augmented.reshape( num_pos_neg, 5)
        Human_augmented_solo = Human_augmented_solo.reshape( num_pos, 5) 
        Object_augmented  = Object_augmented.reshape(num_pos_neg, 5) 
        action_HO         = action_HO.reshape(num_pos_neg, 26) 
        action_H          = action_H.reshape( num_pos, 26)
        mask_HO           = mask_HO.reshape(  num_pos_neg, 26)
        mask_H            = mask_H.reshape(   num_pos, 26)
        H_augmented_encode  = H_encode.reshape( num_pos_neg, 4)
        O_augmented_encode  = O_encode.reshape( num_pos_neg, 4)
        HO_augmented_encode = HO_encode.reshape(num_pos_neg, 4)
    
    return Human_augmented, Human_augmented_solo, Object_augmented, \
            action_HO, action_H, mask_HO, mask_H, H_augmented_encode,O_augmented_encode, HO_augmented_encode



def Generate_action_HICO(action_list):
    action_ = np.zeros(117)
    for GT_idx in action_list:
        action_[GT_idx] = 1
    action_ = action_.reshape(1,117)
    return action_

def Get_Next_Instance_HO_Neg_HICO(trainval_GT, Trainval_Neg, iter, Pos_augment, Neg_select, Data_length):

    GT       = trainval_GT[iter%Data_length]
    image_id = GT[0]
    if Trainval_Neg:
        im_file = cfg.DATA_DIR + '/' + 'hico_20160224_det/images/train2015/HICO_train2015_' + (str(image_id)).zfill(8) + '.jpg'
    else:
        im_file = cfg.DATA_DIR + '/' + 'hico_20160224_det/images/test2015/' + str(image_id) + '.jpg'
    if not os.path.exists(im_file):
        print(iter, im_file)
    im       = cv2.imread(im_file)
    im_orig  = im.astype(np.float32, copy=True)
    im_orig -= cfg.PIXEL_MEANS
    im_shape = im_orig.shape
    im_orig  = im_orig.reshape(1, im_shape[0], im_shape[1], 3)

    Human_augmented, Object_augmented, action_HO, num_pos, H_aug_enc, O_aug_enc, HO_aug_enc = Augmented_HO_Neg_HICO(GT, Trainval_Neg, im_shape, Pos_augment, Neg_select)
    
    blobs = {}
    blobs['image']       = im_orig
    blobs['H_boxes']     = Human_augmented
    blobs['O_boxes']     = Object_augmented
    blobs['gt_class_HO'] = action_HO
    blobs['H_num']       = num_pos
    blobs['H_aug_enc']   = H_aug_enc 
    blobs['O_aug_enc']   = O_aug_enc 
    blobs['HO_aug_enc']  = HO_aug_enc 

    return blobs

def Augmented_HO_Neg_HICO(GT, Trainval_Neg, shape, Pos_augment, Neg_select):
    image_id = GT[0]
    Human    = GT[2]
    Object   = GT[3]
    
    action_HO_ = Generate_action_HICO(GT[1])
    action_HO  = action_HO_
    
    if not Trainval_Neg:
        Human_augmented = np.array([0, Human[0], Human[1], Human[2], Human[3]]).reshape(1,5)
        Human_augmented.astype(np.float64)
        Object_augmented = np.array([0, Object[0], Object[1], Object[2], Object[3]]).reshape(1,5)
        Object_augmented.astype(np.float64)
        
        num_pos = len(Human_augmented)
        num = len(Human_augmented)
        H_encode, O_encode, HO_encode = encode_boxes(Human_augmented[:,1:], Object_augmented[:,1:])
        
        Human_augmented   = Human_augmented.reshape( num, 5 )
        Object_augmented  = Object_augmented.reshape( num, 5 ) 
        action_HO         = action_HO_.reshape(num, 117 )
        H_augmented_encode  = H_encode.reshape( num, 4 )
        O_augmented_encode  = O_encode.reshape( num, 4 )
        HO_augmented_encode = HO_encode.reshape( num, 4 )
        
    else:
        Human_augmented  = Augmented_box(Human,  shape, image_id, Pos_augment)
        Object_augmented = Augmented_box(Object, shape, image_id, Pos_augment)

        Human_augmented  =  Human_augmented[:min(len(Human_augmented),len(Object_augmented))]
        Object_augmented = Object_augmented[:min(len(Human_augmented),len(Object_augmented))]

        num_pos = len(Human_augmented)

        for i in range(num_pos - 1):
            action_HO = np.concatenate((action_HO, action_HO_), axis=0)

        if image_id in Trainval_Neg:

            if len(Trainval_Neg[image_id]) < Neg_select:
                for Neg in Trainval_Neg[image_id]:

                    Human_augmented  = np.concatenate((Human_augmented,  np.array([0, Neg[2][0], Neg[2][1], Neg[2][2], Neg[2][3]]).reshape(1,5)), axis=0)
                    Object_augmented = np.concatenate((Object_augmented, np.array([0, Neg[3][0], Neg[3][1], Neg[3][2], Neg[3][3]]).reshape(1,5)), axis=0)
                    action_HO        = np.concatenate((action_HO, Generate_action_HICO([Neg[1]])), axis=0)
            else:
                List = random.sample(range(len(Trainval_Neg[image_id])), len(Trainval_Neg[image_id]))
                for i in range(Neg_select):
                    Neg = Trainval_Neg[image_id][List[i]]

                    Human_augmented  = np.concatenate((Human_augmented,  np.array([0, Neg[2][0], Neg[2][1], Neg[2][2], Neg[2][3]]).reshape(1,5)), axis=0)
                    Object_augmented = np.concatenate((Object_augmented, np.array([0, Neg[3][0], Neg[3][1], Neg[3][2], Neg[3][3]]).reshape(1,5)), axis=0)
                    action_HO        = np.concatenate((action_HO, Generate_action_HICO([Neg[1]])), axis=0)

        num_pos_neg = len(Human_augmented)

        H_encode, O_encode, HO_encode = encode_boxes(Human_augmented[:,1:], Object_augmented[:,1:])
    
        Human_augmented   = Human_augmented.reshape( num_pos_neg, 5) 
        Object_augmented  = Object_augmented.reshape(num_pos_neg, 5) 
        action_HO         = action_HO.reshape(num_pos_neg, 117) 
        H_augmented_encode  = H_encode.reshape( num_pos_neg, 4)
        O_augmented_encode  = O_encode.reshape( num_pos_neg, 4)
        HO_augmented_encode = HO_encode.reshape(num_pos_neg, 4)

    return Human_augmented, Object_augmented, action_HO, num_pos, H_augmented_encode, O_augmented_encode, HO_augmented_encode

